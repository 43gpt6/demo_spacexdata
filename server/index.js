import path from 'path';
import fs from 'fs';

import React from 'react';
import express from 'express';
import ReactDOMServer from 'react-dom/server';
import axios from 'axios';
import bodyParser from 'body-parser';
import qs from 'querystring';


import App from '../src/App';
import fetch from 'fetch';

const PORT = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/', (req, res) => {
  const app = ReactDOMServer.renderToString(<App />);

  const indexFile = path.resolve('./build/index.html');
  fs.readFile(indexFile, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.get('/api/filter', (req, res) => {
  let url = `${'https://api.spaceXdata.com/v3/launches'}`;
  let filterq = "";
  let currentfilter = Object.keys(req.query);
  let qqee = req.query;
  console.log(qqee, "efdfd");
  if (Object.keys(req.query).length > 0) {
    console.log(currentfilter, "query");
    let yearfilter = "launch_year";
    let launchfilter = "launch_success";
    let landfilter = "land_success";

    if (currentfilter[0] == yearfilter) {
      filterq = "?" + yearfilter + "=" + qqee.launch_year;
    } else if (currentfilter[0] == launchfilter) {
      filterq = "?" + launchfilter + "=" + req.query.launch_success;
    } else if (currentfilter[0] == landfilter) {
      filterq = "?" + landfilter + "=" + req.query.land_success;
    }
  }
  console.log('ul', url + filterq)
  axios.get(url + filterq)
    .then((data) => {
      res.status(200).send(data.data);
    })
})



app.use(express.static('./build'));

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});