import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Router, Route, Link } from 'react-router-dom';

ReactDOM.hydrate(<App />, document.getElementById('root'));