# build environment
FROM node:13.12.0-alpine as builder
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
# install dependencies
RUN npm install
COPY . ./


EXPOSE 8080


# Run the app when the container launches
CMD ["npm", "run", "dev"]

